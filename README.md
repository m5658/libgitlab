# libgitlab

## home, sweet home

    git@gitlab.com:m5658/libgitlab.git
    
## adopt me

    git subtree add  --message 'mtools git subtree add locator - libgitlab\nrepo=git@gitlab.com:m5658/libgitlab.git\nprefix=libgitlab\nbranch=main' --prefix libgitlab  git@gitlab.com:m5658/libgitlab.git main --squash

## inherit updates from the mother ship

    git subtree pull --prefix libgitlab git@gitlab.com:m5658/libgitlab.git main --squash

## push our updates to the mother ship

    git subtree push --prefix libgitlab git@gitlab.com:m5658/libgitlab.git main --squash


